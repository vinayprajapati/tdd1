package com.test

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TaskService)
class TaskServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test servicemethod"() {
        given:"a mocked testservice"
        def testService = Mock(TestService)

        and:"stubbed the serviceMethod"
        1*testService.serviceMethod() >>{
            return 100
        }

        and: "assigned it to taskService"
        service.testService = testService

        when:"called serviceMethod"
        long l = service.serviceMethod()


        then:
        l == 100
    }
}
