package com.tutorials.tdd

import spock.lang.FailsWith
import spock.lang.IgnoreRest
import spock.lang.Specification

class SpockAnnotationSpec extends Specification{

    //TODO:Spock Annotations
//    @Ignore
//    @IgnoreRest
//    @Unroll
//    @Shared
//    @Timeout(1)
    def "test tree Map accepts #key  and is sorted by key  for annotations"(){
        setup:"A sample map"
        Map map = ["deftest":"test2","abtest":"test3"]

        and:"another map"
        Map map3 = new TreeMap()
        map3.putAll(map)
//        Thread.sleep(2000)

        when:"added these values to treemap"
        map3.put(inputKey,"test")

        then:"these must be sorted and accept null"
        map3.toString() == outputMap.toString()

        where:"possible key"
        inputKey <<["null","test22"]
        outputMap << [["abtest":"test3","deftest":"test2",null:"test"],["abtest":"test3","deftest":"test2","test22":"test"]]

    }

    @IgnoreRest
    @FailsWith(IndexOutOfBoundsException) //could be at class level as well
    def ex1() {
        given:
        def foo = []
        foo.get(0)
    }
}
