package com.tutorials.tdd

import spock.lang.Specification

class MapNullValueTestSpec extends Specification {
    //TODO:demonstration of Map accepts null example
    def "HashMap accepts null key"() {
        setup:
        def map = new HashMap()

        when:
        map.put(null, "elem")

        then:
        thrown(NullPointerException)
    }
}
