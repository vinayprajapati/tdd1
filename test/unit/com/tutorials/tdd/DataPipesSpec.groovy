package com.tutorials.tdd

import spock.lang.Specification

class DataPipesSpec extends Specification{
    //TODO:Data Pipes
//    @Timeout(1)
//    @IgnoreRest
//    @Ignore
    def "test tree Map accepts #key  and is sorted by key using datapipes"(){
        setup:
        Map map = ["deftest":"test2","abtest":"test3"]

        and:
        Map map3 = new TreeMap()
        map3.putAll(map)
        System.err.println("=======This ran.")

        when:
        map3.put(inputKey,"test")
//        Thread.sleep(2000)

        then:
        map3.toString() == outputMap.toString()

        where:
        inputKey <<["null","test22"]
        outputMap << [["abtest":"test3","deftest":"test2","null":"test"],["abtest":"test3","deftest":"test2","test22":"test"]]
    }
}
