package com.tutorials.tdd

import com.sun.tools.internal.ws.wsdl.document.Output
import com.tutorials.tdd.enumerations.TaskPriority
import com.tutorials.tdd.enumerations.TaskStatus
import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import spock.lang.IgnoreRest
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Task)
class TaskSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll
    void "test constraints"() {
        given:"an object of task"
        Task task = new Task(name: name ,description: description,startDate: startDate,status: TaskStatus.NEW,category: new Category(),priority: TaskPriority.HIGH)

        when:"validating a task"
        task.validate()

        then:"following validation must happen"
        task.hasErrors()  == output

        where:"following is the input data for different cases"

        name        |description        |startDate      || output
        "test"      |"test"             |new Date()     || false
        null        |"test"             |new Date()     || true
        "test"      |null               |new Date()     || true
        "test"      |"test"             |null           || true
    }

    @Unroll
    void "test save"() {
        given:"an object of task"
        Task task = new Task(name: name ,description: description,startDate: startDate,status: TaskStatus.NEW,category: new Category(),priority: TaskPriority.HIGH).save()

        when:"validating a task"
        int count = Task.count()
        then:"following validation must happen"
        count == output

        where:"following is the input data for different cases"

        name        |description        |startDate      || output
        "test"      |"test"             |new Date()     || 1
        null        |"test"             |new Date()     || 0
        "test"      |null               |new Date()     || 0
        "test"      |"test"             |null           || 0
    }


}
