package com.tutorials.tdd

import spock.lang.Specification

class CodeBlocksSpec extends Specification{
    //TODO:Code blocks examples
    def "test tree Map accepts #key  and is sorted by key using setup"(){
        given:
        Map map = ["deftest":"test2","abtest":"test3"]

        and://not needed actually
        Map map3 = new TreeMap()
        map3.putAll(map)

        when:
        map3.put(inputKey,"test")

        then:
        map3.toString() == outputMap.toString()

        where:
        inputKey = "null"
        outputMap = ["atest":"test3","deftest":"test2","null":"test"]
    }
}
