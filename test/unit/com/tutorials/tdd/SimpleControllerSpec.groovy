package com.tutorials.tdd

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(SimpleController)
class SimpleControllerSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }



    void 'test show book details'() {
        when:
        def model = controller.showBookDetails()
        then:
        model.author == 'Alvin Plantinga'
    }

    void 'test render xml'() {
        when:
        controller.renderXml()
        System.err.println("----$response.text")
        then:
        response.text == "<book title='Great'/>"
        response.xml.@title.text() == 'Great'
        response.contentType == XML_CONTENT_TYPE
    }

    void 'test render json'() {
        when:
        controller.renderJson()
        then:
        response.text == '{"book":"Great"}'
        response.json.book == 'Great'
    }
}
