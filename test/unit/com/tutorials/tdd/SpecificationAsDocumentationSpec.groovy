package com.tutorials.tdd

import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by vinayprajapati on 16/10/15.
 */
class SpecificationAsDocumentationSpec extends Specification {
    //TODO:Specifications as Documentation
    @Unroll
    def "test tree Map accepts #key  and is sorted by key as documentations"(){
        setup:"A sample map"
        Map map = ["deftest":"test2","abtest":"test3"]

        and:"another map"
        Map map3 = new TreeMap()
        map3.putAll(map)

        when:"added these values to treemap"
        map3.put(inputKey,"test")

        then:"these must be sorted and accept null"
        map3.toString() == outputMap.toString()

        where:"possible key"
        inputKey <<["null","test22"]
        outputMap << [["abtest":"test3","deftest":"test2",null:"test"],["abtest":"test3","deftest":"test2","test22":"test"]]

    }
}
