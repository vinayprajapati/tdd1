package com.tutorials.tdd

import spock.lang.Specification

/**
 * Created by vinayprajapati on 16/10/15.
 */
class DataTableSpec extends Specification{

    //TODO:Datatables
//    @IgnoreRest
//    @Unroll
    def "test tree Map accepts #key  and is sorted by key using datatables"(){
        setup:
        Map map = ["deftest":"test2","abtest":"test3"]

        and:
        Map map3 = new TreeMap()
        map3.putAll(map)

        when:
        map3.put(inputKey,"test")

        then:
        map3.toString() == outputMap.toString()

        where:
        inputKey        || outputMap
        "null"          || ["abtest":"test3","deftest":"test2",null:"test"]
        "test22"        || ["abtest":"test3","deftest":"test2","test22":"test"]

    }

}
