package com.task

import com.tutorials.tdd.TaskController
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.web.UrlMappingsUnitTestMixin
import junit.framework.AssertionFailedError
import spock.lang.Specification

//@TestFor(UrlMappings)
@TestMixin(UrlMappingsUnitTestMixin)
@Mock([TaskController])
class UrlMappingSpec extends Specification{


        void "test forward mappings"() {
            when:
            assertForwardUrlMapping("/tasks/show/1", controller: 'task', action: "show")

            then:
            notThrown(AssertionFailedError)
        }
    }
