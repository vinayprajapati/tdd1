package com.task

import com.tutorials.tdd.TaskController
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(TaskController)
@Mock(TaskFilters)
class TaskFiltersSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test list action is filtered"() {
        when:
        withFilters(action:"show") {
            controller.show()
        }
        then:
        response.redirectedUrl == '/category'
    }
}
