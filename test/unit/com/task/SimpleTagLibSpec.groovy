package com.task

import grails.test.mixin.TestFor
import spock.lang.Specification
import com.tutorials.tdd.Task

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(SimpleTagLib)
class SimpleTagLibSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test hello tag"() {
        expect:
        applyTemplate('<s:hello />') == 'Hello World'
    }

    void "test tag calls"() {
        expect:
        tagLib.hello().toString() == 'Hello World'
        tagLib.hello(name: 'Fred').toString() == 'Hello Fred'
        tagLib.bye(task: new Task(name: 'Fred')) == 'Bye Fred'
    }
}
