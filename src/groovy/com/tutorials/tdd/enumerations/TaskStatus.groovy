package com.tutorials.tdd.enumerations

enum TaskStatus{
    NEW,PENDING,DELAYED,POSTPONED,ARCHIVED,DELETED,COMPLETED
}